const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController")
const { route } = require("./userRoutes.js")

/* 
    Activity
    Create a reoute that will let an admin perform addCourse function in the courseController

        verify that the user is logged in
        decode the token for that usser
        use the id, isAdmin, and request body to perform the function in the courseController

            the id and isAdmin are parrts of an object
*/

/* 

s34 business logic (courseRoutes)
ACTIVITY
create a route that will let an admin perform addCourse function in the courseController
    verify that the user is logged in
    decode the token for that user
    use the id, isAdmin, and request body to perform the function in courseController
        the id and isAdmin are parts of an object
Business Logic (courseController)
ACTIVITY s34
    1. find the user in the database
        find out if the user is admin

    2. if the user is not an admin, send the response "You are not an admin"

    3. if the user is an admin, create the new Course object
            -save the object
                -if there are errors, return false
                -if there are no errors, return "Course created sucessfully"

*/

router.post("/", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})

/* 

e commerce websites

*/

/* 
create a route that will retrieve  all of our products/courses
    will require login/register functions
*/

router.get("/",(req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})
module.exports = router;